use std::env;
use std::convert::TryFrom;
use std::sync::RwLock;
use iron::mime::Mime;
use iron::prelude::*;
use iron::status;
use router::Router;
use rand::Rng;

mod collage;
mod image;

fn walk_dir(dir: &str) -> Vec<image::Image> {
    let dirlist: Vec<String> = glob::glob(&format!("{}/*", dir))
        .expect("Can't load requested folder")
        .filter(|x| x.is_ok())
        .map(|x| x.unwrap().to_str().map(|x| String::from(x)))
        .filter(|x| x.is_some()).map(|x| x.unwrap())
        .filter(|x| std::fs::metadata(x).unwrap().is_dir())
        .collect();
    let mut dir_content: Vec<image::Image> = glob::glob(&format!("{}/*", dir))
        .expect("Can't load requested folder")
        .filter(|x| x.is_ok())
        .map(|x| x.unwrap().to_str().map(|x| String::from(x)))
        .filter(|x| x.is_some())
        .map(|x| image::Image::try_from(x.unwrap()))
        // Now check which ones are images
        .filter(|x| x.is_ok())
        .map(|x| x.unwrap())
        .collect();
    for directory in dirlist {
        dir_content.append(&mut walk_dir(&directory));
    }
    dir_content
}

fn main() {
    let directory = env::args().nth(1).expect("Run with folder path");
    let dir_content: RwLock<Vec<image::Image>> = RwLock::new(walk_dir(&directory));
    println!("Server ready! Found {} images.", dir_content.read().unwrap().len());
    
    let handler = move|req: &mut Request| {
        let ref w_str = req.extensions.get::<Router>().unwrap().find("w").unwrap();
        let ref h_str = req.extensions.get::<Router>().unwrap().find("h").unwrap();
        let ref n_str = req.extensions.get::<Router>().unwrap().find("n").unwrap();
        let w = w_str.parse::<usize>().unwrap_or(0usize);
        let h = h_str.parse::<usize>().unwrap_or(0usize);
        let mut n = n_str.parse::<usize>().unwrap_or(0usize);
        let imgs = dir_content.read().unwrap();
        if n == 0 {
            n = rand::thread_rng().gen_range(0, imgs.len()-1);
        } else {
            n = n % imgs.len();
        }
        println!("{}", n);
        let mut iterator = imgs.iter();
        for _ in 0 .. n {
            iterator.next();
        }
        let (collage, number_of_images) = collage::collage(w, h, iterator);
        let raw_pixels = collage.display();
        let output = image::RawImage{size: (w, h), bytes: raw_pixels};

        let mut resp = Response::with((
            "image/png".parse::<Mime>().unwrap(),
            status::Ok,
            output.as_png(),
        ));
        resp.headers.set_raw("X-Last-Served", vec![format!("{}", n + number_of_images).as_bytes().to_vec()]);
        resp.headers.set_raw("Cache-Control", vec!["public, max-age=604800, immutable".as_bytes().to_vec()]);
        Ok(resp)
    };
    fn webpage(_req: &mut Request) -> IronResult<Response> {
        Ok(Response::with((
            "text/html".parse::<Mime>().unwrap(),
            status::Ok,
            include_str!("../index.html"),
        )))
    }

    let mut router = Router::new();           // Alternative syntax:
    router.get("/:w/:h/:n", handler, "query");  // query: get "/:query" => handler);
    router.get("/", webpage, "home");
    Iron::new(router).http("0.0.0.0:3000").unwrap();
}
