use std::convert::TryFrom;
use mime_guess::mime::*;
use imagefmt::ColFmt;
use mtpng::*;
use rustiff::{
    Decoder,
    ImageData,
};

#[derive(PartialEq)]
pub enum Orientation {
    Portrait,
    Landscape,
    Square,
}

impl From<(usize, usize)> for Orientation {
    fn from(s: (usize, usize)) -> Self {
        if s.0 == s.1 {
            Self::Square
        } else if s.0 < s.1 {
            Self::Portrait
        } else {
            Self::Landscape
        }
    }
}

#[derive(Debug, Clone)]
pub enum Image {
    Jpeg(String),
    Tiff(String),
}

#[derive(Debug)]
pub struct RawImage {
    pub size: (usize, usize),
    pub bytes: Vec<u8>,
}

impl RawImage {
    pub fn as_png(&self) -> Vec<u8> {
        let writer = Vec::<u8>::new();
    
        let mut header = Header::new();
        header.set_size(self.size.0 as u32, self.size.1 as u32).unwrap();
        header.set_color(ColorType::Truecolor, 8).unwrap();
        let options = encoder::Options::new();
    
        let mut encoder = encoder::Encoder::new(writer, &options);
    
        encoder.write_header(&header).unwrap();
        encoder.write_image_rows(&self.bytes).unwrap();
        encoder.finish().unwrap()
    }
    pub fn resize(&self, w: usize, h: usize) -> Self {
        use resize::Pixel::RGB24;
        use resize::Type::{Lanczos3, Mitchell};

        // Destination buffer. Must be mutable.
        let mut dst = vec![0u8;w*h*3];
        // Create reusable instance.
        let mut resizer;
        if w.min(h) < self.size.0.min(self.size.1) {
            resizer = resize::new(self.size.0, self.size.1, w, h, RGB24, Lanczos3);
        } else {
            resizer = resize::new(self.size.0, self.size.1, w, h, RGB24, Mitchell);
        }
        // Do resize without heap allocations.
        // Might be executed multiple times for different `src` or `dst`.
        resizer.resize(&self.bytes, &mut dst);
        Self{size:(w, h), bytes: dst}
    }
    /// This function will resize the image so that it will fit inside a rectangle(w, h)
    /// The function can crop the image but not distorce it
    pub fn resize_to_fit(&self, w: usize, h: usize) -> Self {
        let (hresize_w, hresize_h) = (self.size.0 * h / self.size.1, h);
        let (wresize_w, wresize_h) = (w, self.size.1 * w / self.size.0);
        let (final_w, final_h);
        if hresize_w < w {
            final_w = wresize_w;
            final_h = wresize_h;
        } else {
            final_w = hresize_w;
            final_h = hresize_h;
        }
        let (margin_w, margin_h) = ((final_w - w) / 4, (final_h - h) / 4);
        self.resize(final_w, final_h).crop((margin_w, margin_h), (w + margin_w, h + margin_h))
    }
    pub fn crop(&self, top_left: (usize, usize), bottom_right: (usize, usize)) -> Self {
        let mut res: RawImage = Self{ size:(bottom_right.0-top_left.0,bottom_right.1-top_left.1), bytes: vec![0; 3*(bottom_right.0-top_left.0)*(bottom_right.1-top_left.1)]};
        // either this (should be faster but idk)
        for y in 0..res.size.1 {
            for x in 0..res.size.0 {
                res.bytes[3*(y*res.size.0+x)] = self.bytes[3*((top_left.1+y)*self.size.0 + top_left.0+x)];
                res.bytes[3*(y*res.size.0+x) + 1] = self.bytes[3*((top_left.1+y)*self.size.0 + top_left.0+x) + 1];
                res.bytes[3*(y*res.size.0+x) + 2] = self.bytes[3*((top_left.1+y)*self.size.0 + top_left.0+x) + 2];
            }
          }
      res
    }
        pub fn orientation(&self) -> Orientation {
        self.size.into()
    }
}

impl From<&Image> for RawImage {
    fn from(img: &Image) -> Self {
        match img {
            Image::Jpeg(path) => {
                let pic = imagefmt::read(&path, ColFmt::RGB).unwrap();
                Self{size: (pic.w, pic.h), bytes: pic.buf}
            },
            Image::Tiff(path) => {
                let f = std::fs::File::open(&path).unwrap();
                let mut decoder = Decoder::new(f).unwrap();
                let image = decoder.image().unwrap();
                let image_data = image.data();
                let ifd = decoder.ifd().unwrap();
                let width = decoder.get_value(&ifd, rustiff::tag::ImageWidth).unwrap();
                let height = decoder.get_value(&ifd, rustiff::tag::ImageLength).unwrap();
                if let ImageData::U8(buf) = image_data {
                    Self{size: (width as usize, height as usize), bytes: buf.clone()}
                } else {
                    panic!("16 bit TIFF found");
                }
            }
        }
    }
}

impl TryFrom<String> for Image {
    type Error = &'static str;
    fn try_from(path: String) -> Result<Self, Self::Error> {
        match mime_guess::from_path(&path).first() {
            Some(x) => if x == IMAGE_JPEG || x ==IMAGE_PNG {
                Ok(Image::Jpeg(path))
            } else if x.to_string() == "image/tiff" {
                Ok(Image::Tiff(path))
            } else {
                Err("Not an image")
            },
            None => Err("Not an image")
        }
    }
}

