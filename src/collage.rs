use super::image;
use std::slice::Iter;

pub trait Pixel {
    fn display(&self) -> Vec<u8>;
}

pub struct VStack(Vec<Box<dyn Pixel>>);
pub struct HStack((Vec<Box<dyn Pixel>>, usize));

impl Pixel for HStack {
    fn display(&self) -> Vec<u8> {
        print!("Hstack<");
        let mut res = Vec::new();

        // Converting all the pieces to pixels
        let mut pixels = Vec::new();
        for image in &(self.0).0 {
            let p = image.display();
            pixels.push((p.len() / (self.0).1, p));
        }

        let num_rows = (self.0).1 as usize;
        for i in 0 .. num_rows {
            let mut temp = Vec::new();
            for image in &pixels {
                temp.append(&mut Vec::from(&image.1[i*image.0 .. (i + 1)*image.0]));
            }
            res.append(&mut temp);
        }
        print!(">");
        res
    }
}

impl Pixel for VStack {
    fn display(&self) -> Vec<u8> {
        let mut res = Vec::new();
        print!("VStack<");
        for piece in &self.0 {
            res.append(&mut piece.display());
        }
        print!(">");
        res
    }
}

impl Pixel for image::RawImage {
    fn display(&self) -> Vec<u8> {
        print!(" Image{:?} ", self.size);
        self.bytes.clone()
    }
}


pub fn collage(w: usize, h: usize, mut images: Iter<image::Image>) -> (Box<dyn Pixel>, usize) {
    let collage_ratio = w as f64 / h as f64;
    let res: Box<dyn Pixel>;
    let length;
    let image = images.next().unwrap();
    println!("{:?}, {}", image, images.len());
    let raw_image = image::RawImage::from(image);
    let ratio = raw_image.size.0 as f64 / raw_image.size.1 as f64;
    if w == 0 || h == 0 {
        res = Box::new(VStack(vec![]));
        length = 0;
    }else if images.len() == 0 || (1.0 - (ratio / collage_ratio)).abs() < 0.1 || w < 100 || h < 100 {
        // If it is the last image in the iterator or if the image has similiar proportion (<10% difference)
        res = Box::new(raw_image.resize_to_fit(w, h));
        length = 1
    } else {
        if raw_image.orientation() == image::Orientation::Portrait {
            let (mut img_w, img_h) = (raw_image.size.0 * h / raw_image.size.1, h);
            let new_image;
            if img_w > w {
                img_w = w;
                new_image = raw_image.resize_to_fit(img_w, img_h);
            } else {
                new_image = raw_image.resize(img_w, img_h);
            }
            let (frame_w, frame_h) = (w - img_w, h);
            let mut tmp_res: Vec<Box<(dyn Pixel + 'static)>> = vec![Box::new(new_image)];
            let mut tmp_length = 1;
            if frame_w > 0 {
                let tmp_collage = collage(frame_w, frame_h, images);
                tmp_res.push(tmp_collage.0);
                tmp_length += tmp_collage.1;
            }
            res = Box::new(HStack((tmp_res, h)));
            length = tmp_length
        } else {
            let (img_w, mut img_h) = (w, raw_image.size.1 * w / raw_image.size.0);
            let new_image;
            if img_h > h {
                img_h = h;
                new_image = raw_image.resize_to_fit(img_w, img_h);
            } else {
                new_image = raw_image.resize(img_w, img_h);
            }
            let (frame_w, frame_h) = (w, h - img_h);
            let mut tmp_res: Vec<Box<(dyn Pixel + 'static)>> = vec![Box::new(new_image)];
            let mut tmp_length = 1;
            if frame_w > 0 {
                let tmp_collage = collage(frame_w, frame_h, images);
                tmp_res.push(tmp_collage.0);
                tmp_length += tmp_collage.1;
                if tmp_collage.1 == 0 {
                    tmp_length -= 1;
                }
            }
            res = Box::new(VStack(tmp_res));
            length = tmp_length
        }
    }
    (res, length)
}